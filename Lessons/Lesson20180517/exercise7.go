package main

// add Delete latest entry
// add ClearALL
// Questions
// 1/ Why Add(), why not add()? or it is a naming convention?
// 2/ Is it the right way to use the delCheck field to check certain status?

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"
)

// MaxHistory is the number of history events tracked. To address "green" error after saving
const (
	LuckyMaxHistory = 8 // testing purpose
)

// HistoryResp data structure -> History -> HistoryEntries
type HistoryResp struct {
	CurLuckyNum     int            `json:"current_lucky_num"`
	LuckyNumHistory []HistoryEntry `json:"lucky_num_history"`
}

type History struct {
	HistoryEntries []HistoryEntry
	maxHistory     int
	runCounter     int
	delCheck       string // PK
}

type HistoryEntry struct {
	HistRun   int       `json:"hist_run"`
	RandNum   int       `json:"lucky_num"`
	VisitedAt time.Time `json:"visited_at"`
}

// NewHistory function  to initialise and create History slice
func NewHistory(maxHistory int) (*History, error) {
	if maxHistory <= 0 {
		return nil, errors.New("maxHistory must be greater than 0")
	}

	history := History{
		maxHistory:     maxHistory,
		HistoryEntries: make([]HistoryEntry, 0), // "0" to avoid displaying misleading info in incomplete history during initial run, eg., less than 3 records
	}

	return &history, nil
}

// Add function to append history entry https://tour.golang.org/moretypes/15
func (h *History) Add(luckyNum int) {

	if len(h.HistoryEntries) == h.maxHistory { // housekeeping on Histroy Entry
		h.HistoryEntries = h.HistoryEntries[1:h.maxHistory]
	}
	t := time.Now()
	h.runCounter++
	h.delCheck = "N"
	h.HistoryEntries = append(h.HistoryEntries, HistoryEntry{
		HistRun:   h.runCounter,
		RandNum:   luckyNum,
		VisitedAt: t,
	})

}

func (h *History) RemoveOldestEntry() {

	if len(h.HistoryEntries) == 1 {
		h.HistoryEntries = h.HistoryEntries[:0]

	} else {
		if (h.delCheck == "N") && (len(h.HistoryEntries) >= h.maxHistory) { // check "=" is good. see Add(), but safer with >=
			// should I use such a "delCheck" indicator for use in 1st pass??
			h.HistoryEntries = h.HistoryEntries[0 : h.maxHistory-1] // oldest history has already gone in Add() at maxHistory

		} else {
			if (h.delCheck == "N") && (len(h.HistoryEntries) < h.maxHistory) {
				h.HistoryEntries = h.HistoryEntries[1 : len(h.HistoryEntries)-1] // remove Entry that was not displayed, see Add()

			} else {
				h.HistoryEntries = h.HistoryEntries[1:len(h.HistoryEntries)]
			}
		}

	}
	h.delCheck = "Y" // 1st DEL pass done. will reset to "N" in Add() when Add() is called
}
func (h *History) ClearALL() {

	h.HistoryEntries = h.HistoryEntries[:0]
}

func main() {
	luckyHistory, err := NewHistory(LuckyMaxHistory)
	if err != nil {
		panic(err)
	}

	http.HandleFunc("/api/lucky", newLuckyAPIHandler(luckyHistory))
	http.HandleFunc("/api/lucky/removeOldest", del1HistoryHandler(luckyHistory))
	http.HandleFunc("/api/lucky/clear", clrallHistoryHandler(luckyHistory))

	fmt.Println("starting HTTP server")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func newLuckyAPIHandler(history *History) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		curLuckyNum := rand.Intn(100)

		resp := HistoryResp{
			CurLuckyNum:     curLuckyNum,
			LuckyNumHistory: history.HistoryEntries,
		}

		respBytes, err := json.Marshal(resp)
		if err != nil {
			panic(err)
		}
		w.Write(respBytes)

		history.Add(curLuckyNum)
	}
}

//++ start of Exercie7.go

func del1HistoryHandler(h *History) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		if len(h.HistoryEntries) > 0 {
			h.RemoveOldestEntry()

			resp := HistoryResp{
				LuckyNumHistory: h.HistoryEntries,
			}

			respBytes, err := json.Marshal(resp.LuckyNumHistory) // not appropriate to show number 0 as no further generation
			if err != nil {
				panic(err)
			}

			if len(h.HistoryEntries) > 0 {
				w.Write(respBytes)
			} else {
				w.Write([]byte("All Historical entires deleted"))
			}
		} else {
			w.Write([]byte("Oops.... Nothing to delete"))
		}
	}
}

func clrallHistoryHandler(h *History) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if len(h.HistoryEntries) == 0 {
			w.Write([]byte("No history entries to clear"))
		} else {
			h.ClearALL()
			resp := HistoryResp{
				LuckyNumHistory: h.HistoryEntries,
			}

			respBytes, err := json.Marshal(resp.LuckyNumHistory)
			if err != nil {
				panic(err)
			}
			w.Write([]byte("All historical entries cleared\n"))
			w.Write(respBytes) // to confirm actual deletion

		}
	}

}

//++ end of Exercie7.go
