package main

// question
// (1) Do not need Seed for rand? rand.Intn is sufficient?
//     ref: https://stackoverflow.com/questions/12321133/golang-random-number-generator-how-to-seed-properly
// (2) Is the generated time format correct?

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"
)

// MaxHistory is the number of history events tracked. To address "green" error after saving
const (
	LuckyMaxHistory = 3
)

type HistoryResp struct {
	CurLuckyNum     int            `json:"current_lucky_num"`
	LuckyNumHistory []HistoryEntry `json:"lucky_num_history"`
}

type History struct {
	HistoryEntries []HistoryEntry
	maxHistory     int
	runCounter     int
}

type HistoryEntry struct {
	HistRun   int       `json:"hist_run"` //pk
	RandNum   int       `json:"lucky_num"`
	VisitedAt time.Time `json:"visited_at"` // change from time.Time to string for formatting purpose
	//	VisitedAt time.Time `json:"visited_at"`
}

// NewHistory function  to initialise and create History slice
func NewHistory(maxHistory int) (*History, error) {
	if maxHistory <= 0 {
		return nil, errors.New("maxHistory must be greater than 0")
	}

	history := History{
		maxHistory:     maxHistory,
		HistoryEntries: make([]HistoryEntry, 0), // to avoid displaying misleading info in incomplete history during initial run, eg., less than 3 records
	}

	return &history, nil
}

// Add function to append history entry https://tour.golang.org/moretypes/15
func (h *History) Add(luckyNum int) {
	// https://golang.org/pkg/time/ Const RFC3339 etc
	// A decimal point followed by one or more zeros represents a fractional second, printed to the given number of decimal places.
	t := time.Now()
	h.runCounter++
	h.HistoryEntries = append(h.HistoryEntries, HistoryEntry{
		HistRun:   h.runCounter,
		RandNum:   luckyNum,
		VisitedAt: t,
	})

	if len(h.HistoryEntries) > h.maxHistory {
		h.HistoryEntries = h.HistoryEntries[1 : h.maxHistory+1] // in reverse sequence? new one last due to "Append"?
	}
}

func main() {
	luckyHistory, err := NewHistory(LuckyMaxHistory)
	if err != nil {
		panic(err)
	}

	http.HandleFunc("/api/lucky", newLuckyAPIHandler(luckyHistory))
	fmt.Println("starting HTTP server")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

// this is a function that returns a function
func newLuckyAPIHandler(history *History) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		curLuckyNum := rand.Intn(100) // latest rand by itself. Need Seed()
		resp := HistoryResp{
			CurLuckyNum:     curLuckyNum,
			LuckyNumHistory: history.HistoryEntries,
		}

		respBytes, err := json.Marshal(resp)
		if err != nil {
			panic(err)
		}

		w.Write(respBytes)

		history.Add(curLuckyNum)
	}
}
