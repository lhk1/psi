// https://golang.org/ search by Go tutotial video
// 29/4/2018
// You can edit this code!
// Click here and start typing.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"net/http"
	"time"
)

func main() {

	http.HandleFunc("/api/psi", neapsi) // - format date/time

	http.HandleFunc("/chair", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "chair")
	})
	http.HandleFunc("/lpk1", barhandler)  // - print text via Func
	http.HandleFunc("/ftime", formattime) // - format date/time
	http.HandleFunc("/wpost", wstpost)
	log.Fatal(http.ListenAndServe(":8080", nil))
	// wstpost()
}

func neapsi(w http.ResponseWriter, r *http.Request) {
	type psiStruct struct {
		RegionMetadata []struct {
			Name          string `json:"name"`
			LabelLocation struct {
				Latitude  float64 `json:"latitude"`
				Longitude float64 `json:"longitude"`
			} `json:"label_location"`
		} `json:"region_metadata"`
		Items []struct {
			Timestamp       time.Time `json:"timestamp"`
			UpdateTimestamp time.Time `json:"update_timestamp"`
			Readings        struct {
				O3SubIndex struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"o3_sub_index"`
				Pm10TwentyFourHourly struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"pm10_twenty_four_hourly"`
				Pm10SubIndex struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"pm10_sub_index"`
				CoSubIndex struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"co_sub_index"`
				Pm25TwentyFourHourly struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"pm25_twenty_four_hourly"`
				So2SubIndex struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"so2_sub_index"`
				CoEightHourMax struct {
					West     float64 `json:"west"`
					National float64 `json:"national"`
					East     float64 `json:"east"`
					Central  float64 `json:"central"`
					South    float64 `json:"south"`
					North    float64 `json:"north"`
				} `json:"co_eight_hour_max"`
				No2OneHourMax struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"no2_one_hour_max"`
				So2TwentyFourHourly struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"so2_twenty_four_hourly"`
				Pm25SubIndex struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"pm25_sub_index"`
				PsiTwentyFourHourly struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"psi_twenty_four_hourly"`
				O3EightHourMax struct {
					West     int `json:"west"`
					National int `json:"national"`
					East     int `json:"east"`
					Central  int `json:"central"`
					South    int `json:"south"`
					North    int `json:"north"`
				} `json:"o3_eight_hour_max"`
			} `json:"readings"`
		} `json:"items"`
		APIInfo struct {
			Status string `json:"status"`
		} `json:"api_info"`
	}
	resp, _ := http.Get("https://api.data.gov.sg/v1//environment/psi")
	bytes, _ := ioutil.ReadAll(resp.Body)
	stringbody := string(bytes)
	fmt.Fprintln(w, stringbody)
	resp.Body.Close()

	// how to pick up an item of Struct ....30/4 10:12am

}

func barhandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "this is LPK1 with time.Now()")
	t := time.Now()

	//	fmt.Fprintln(w, time.Now())
	fmt.Fprintln(w, t)
	// https://stackoverflow.com/questions/20234104/how-to-format-current-time-using-a-yyyymmddhhmmss-format
	const layout = "Monday, January 2, 2006 at 3:04pm (MST)"
	fmt.Fprintln(w, t.Format(layout)) // must be 2006?
}

func wstpost(w http.ResponseWriter, r *http.Request) {
	// https://www.youtube.com/watch?v=ccANcNk8Dac - news aggregator tutorial 10
	resp, _ := http.Get("https://www.washingtonpost.com/news-sitemap-index.xml")
	bytes, _ := ioutil.ReadAll(resp.Body)
	stringbody := string(bytes)
	fmt.Println(stringbody)     // display on console
	fmt.Fprintln(w, stringbody) // display on browser
	resp.Body.Close()
}

func add(x, y float64) float64 {
	// var num1, num2 float64 = 5.6, 9.4
	// fmt.Println("sum of num1 & num2 is ", add(num1, num2))
	// fmt.Printf("the two numbers are %f and %f ", num1, num2)
	return x + y

}
func start() {

	fmt.Println("Hello, 世界")
	fmt.Println("The square root of 4 is ", math.Sqrt(4))
	fmt.Println("The Random number 1-100", rand.Intn(100))
}

func formattime(w http.ResponseWriter, r *http.Request) {
	// p := fmt.Println
	p := fmt.Fprintln
	t := time.Now()
	p(w, "(1) ", t.Format(time.RFC3339))

	t1, e := time.Parse(
		time.RFC3339,
		"2012-11-01 22:08:41+00:00") // output 0001-01-01 00:00:00 +0000 UTC??
	p(w, "(2) ", t1)

	p(w, "(3) ", t.Format("3:04PM"))
	p(w, "(4) ", t.Format("Mon Jan _2 15:04:05 2006")) // must it be 2006?
	// Output (4)  Thu May  3 12:30:30 2018 when year is 2006
	// Output (4)  Thu May  3 12:32:18 3007 when year is 2007
	p(w, "(5) ", t.Format("2006-01-02 15:04:05.999999-07:00"))
	form := "3 04 PM"
	t2, e := time.Parse(form, "8 41 PM")
	p(w, "(6) ", t2)

	fmt.Fprintf(w, "(7) %d-%02d-%02d T %02d:%02d:%02d-00:00\n",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())

	ansic := "Mon Jan _2 15:04:05 2006"
	_, e = time.Parse(ansic, "8:41PM")
	p(w, "(8) ", e)

	// the output
	// 	(1)  2018-04-26T20:31:03+08:00
	// (2)  0001-01-01 00:00:00 +0000 UTC
	// (3)  8:31PM
	// (4)  Thu Apr 26 20:31:03 2018
	// (5)  2018-04-26 20:31:03.069325+08:00
	// (6)  0000-01-01 20:41:00 +0000 UTC
	// (7) 2018-04-26 T 20:31:03-00:00
	// (8)  parsing time "8:41PM" as "Mon Jan _2 15:04:05 2006": cannot parse "8:41PM" as "Mon"
}

// Questions
// how to have new line in Println, or insert variables
// try - fmt.Fprintf (w, "<p> You %s even add %s</p>,"can","<strong> variables</strong>)
// (or Printf) this works fmt.Printf("the two numbers are %f and %f ", num1, num2)
