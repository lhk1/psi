package main

// questions
// (1) do not need Seed for rand? rand.Intn is sufficient?
// (2) how to format the time as "2018-05-15T05:09:08.423486405Z"
// (3) how to have the latest random number display in history first.. (append is to the back)

// follow up works - 15/5/2018
// display only 3 entries: 1 new number with 3 history
//
// Whenever the route is visited, your program should return a JSON response containing:
// 1) a lucky number from 0-99; and
// 2) the last 3 lucky numbers generated, and the time on which they were generated.

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"
	// pk try to format time
)

// MaxHistory is the number of history events tracked
const (
	Lucky1MaxHistory = 3
	Lucky2MaxHistory = 7
)

// History define history struct
type History struct {
	CurrentLuckyNum int            `json:"current_lucky_num"` // pk
	HistoryEntries  []HistoryEntry `json:"lucky_num_history"` //pk
	maxHistory      int
}

// HistoryEntry define history entry struct
type HistoryEntry struct {
	HistRun   int       `json:"hist_run"` //pk
	RandNum   int       `json:"lucky_num"`
	VisitedAt time.Time `json:"visited_at"` // how to format the time in Struct
}

// NewHistory function  to create historical records
func NewHistory(maxHistory int) (*History, error) {

	if maxHistory <= 0 {
		return nil, errors.New("maxHistory must be greater than 0")
	}

	history := History{
		maxHistory:     maxHistory,
		HistoryEntries: make([]HistoryEntry, 0), // to avoid displaying misleading info in incomplete history during initial run, eg., less than 3 records
	}

	return &history, nil
}

// Add function to append history entry https://tour.golang.org/moretypes/15
func (h *History) Add(cnt, luckyNum int) {
	h.HistoryEntries = append(h.HistoryEntries, HistoryEntry{
		HistRun: cnt, // pk
		RandNum: luckyNum,

		// try to format the time

		VisitedAt: time.Now(),
	})
	h.CurrentLuckyNum = luckyNum // pk
	if len(h.HistoryEntries) > h.maxHistory {
		h.HistoryEntries = h.HistoryEntries[1 : h.maxHistory+1] // in reverse sequence? new one last due to "Append"?
	}
}

func main() {

	http.HandleFunc("/api/lucky", luckyAPIHandler)
	fmt.Println("starting HTTP server")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
func luckyAPIHandler(w http.ResponseWriter, r *http.Request) {
	lucky1History, _ := NewHistory(Lucky1MaxHistory)
	for i := 0; i < Lucky1MaxHistory; i++ {
		lucky1History.Add(i, rand.Intn(100))

	}
	lucky1History.CurrentLuckyNum = rand.Intn(100) // latest rand by itself

	resp, err := json.Marshal(lucky1History)
	//	fmt.Println(i, string(resp))

	if err != nil {
		panic(err)
	}

	w.Write(resp)

}
