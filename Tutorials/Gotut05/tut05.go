package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {

	// changes after GIT INIT
	// for Exercise #4 - must "Go Build tut05.go psiType.go && tut05"  - to merge all files.... no sure now
	// https://stackoverflow.com/questions/10838469/how-to-compile-go-program-consisting-of-multiple-files

	neapsis := NeaPsi{}

	resp, _ := http.Get("https://api.data.gov.sg/v1//environment/psi")
	bytes, _ := ioutil.ReadAll(resp.Body)

	err1 := json.Unmarshal(bytes, &neapsis)
	if err1 != nil {
		fmt.Println("error:", err1)
	}

	// concatenate strings... https://play.golang.org/ Not working 3/5/2018
	// const a = "neapsis.Items[0].Readings.Pm10TwentyFourHourly."
	// output := strings.Join([]string{a, "National"}, "")
	// fmt.Println(output)
	// fmt.Printf("\n PSI 24H - National: %d", &output)

	// Exercise output
	fmt.Printf("\n PSI 24H - National: %d", neapsis.Items[0].Readings.PsiTwentyFourHourly.National)
	fmt.Printf(", North: %d", neapsis.Items[0].Readings.PsiTwentyFourHourly.North)
	fmt.Printf(", East: %d", neapsis.Items[0].Readings.PsiTwentyFourHourly.East)
	fmt.Printf(", South: %d", neapsis.Items[0].Readings.PsiTwentyFourHourly.South)
	fmt.Printf(", West: %d", neapsis.Items[0].Readings.PsiTwentyFourHourly.West)

	fmt.Printf("\n <1> PM10 National is %d", neapsis.Items[0].Readings.Pm10TwentyFourHourly.National)
	fmt.Printf("\n <2> PM10 North is %d", neapsis.Items[0].Readings.Pm10TwentyFourHourly.North)
	fmt.Printf("\n <3> PM10 East is %d", neapsis.Items[0].Readings.Pm10TwentyFourHourly.East)
	fmt.Printf("\n <4> PM10 South is %d", neapsis.Items[0].Readings.Pm10TwentyFourHourly.South)
	fmt.Printf("\n <5> PM10 West is %d", neapsis.Items[0].Readings.Pm10TwentyFourHourly.West)

	fmt.Printf("\n <6> PSI National is %d", neapsis.Items[0].Readings.PsiTwentyFourHourly.National)
	fmt.Printf("\n <7> PSI North is %d", neapsis.Items[0].Readings.PsiTwentyFourHourly.North)
	fmt.Printf("\n <8> PSI East is %d", neapsis.Items[0].Readings.PsiTwentyFourHourly.East)
	fmt.Printf("\n <9> PSI South is %d", neapsis.Items[0].Readings.PsiTwentyFourHourly.South)
	fmt.Printf("\n <10> PSI West is %d", neapsis.Items[0].Readings.PsiTwentyFourHourly.West)

	// can I save this "neapsis.Items[0].Readings" as text to append? this following does not work
	// type prtext []string
	// const a = "neapsis.Items[0].Readings.Pm10TwentyFourHourly."
	// output := strings.Join([]string{a, "National"}, "")
	// fmt.Println(output)

	// "PSI 24H - North: 30, South 43, .

	// 	var jsonBlob = []byte(`[
	// 	{"Name": "Platypus", "Order": "Monotremata"},
	// 	{"Name": "Quoll",    "Order": "Dasyuromorphia"}
	// ]`)
	// 	type Animal struct {
	// 		Name  string
	// 		Order string
	//	}
	// var animals []Animal
	// err := json.Unmarshal(jsonBlob, &animals)
	// if err != nil {
	// 	fmt.Println("error:", err)
	// }
	// fmt.Printf("%+v", animals)
}
