package main

// https://blog.alexellis.io/golang-json-api-client/
// How to extract the value in a nested Struct

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type people struct {
	Number int `json:"number"`
}

// type people struct {
// 	Name string `json:"name"`
// }

type neaPsi struct {
	RegionMetadata []struct {
		Name          string `json:"name"`
		LabelLocation struct {
			Latitude  float64 `json:"latitude"`
			Longitude float64 `json:"longitude"`
		} `json:"label_location"`
	} `json:"region_metadata"`
	Items []struct {
		Timestamp       time.Time `json:"timestamp"`
		UpdateTimestamp time.Time `json:"update_timestamp"`
		Readings        struct {
			O3SubIndex struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"o3_sub_index"`
			Pm10TwentyFourHourly struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"pm10_twenty_four_hourly"`
			Pm10SubIndex struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"pm10_sub_index"`
			CoSubIndex struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"co_sub_index"`
			Pm25TwentyFourHourly struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"pm25_twenty_four_hourly"`
			So2SubIndex struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"so2_sub_index"`
			CoEightHourMax struct {
				West     float64 `json:"west"`
				National float64 `json:"national"`
				East     float64 `json:"east"`
				Central  float64 `json:"central"`
				South    float64 `json:"south"`
				North    float64 `json:"north"`
			} `json:"co_eight_hour_max"`
			No2OneHourMax struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"no2_one_hour_max"`
			So2TwentyFourHourly struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"so2_twenty_four_hourly"`
			Pm25SubIndex struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"pm25_sub_index"`
			PsiTwentyFourHourly struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"psi_twenty_four_hourly"`
			O3EightHourMax struct {
				West     int `json:"west"`
				National int `json:"national"`
				East     int `json:"east"`
				Central  int `json:"central"`
				South    int `json:"south"`
				North    int `json:"north"`
			} `json:"o3_eight_hour_max"`
		} `json:"readings"`
	} `json:"items"`
	APIInfo struct {
		Status string `json:"status"`
	} `json:"api_info"`
}

func main() {

	nested() // http://www.golangprograms.com/how-to-unmarshal-nested-json-structure.html

	url := "http://api.open-notify.org/astros.json"
	// url := "https://api.data.gov.sg/v1//environment/psi"

	spaceClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "spacecount-tutorial")

	res, getErr := spaceClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	people1 := people{}
	jsonErr := json.Unmarshal(body, &people1)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	fmt.Println(people1.Number)
	// fmt.Println(neaPsi11.Items.Pm10TwentyFourHourly.National) - ERROR: how to select individual field
}

// "http://api.open-notify.org/astros.json"
// {"people": [{"name": "Anton Shkaplerov", "craft": "ISS"}, {"name": "Scott Tingle", "craft": "ISS"}, {"name": "Norishige Kanai", "craft": "ISS"}, {"name": "Oleg Artemyev", "craft": "ISS"}, {"name": "Andrew Feustel", "craft": "ISS"}, {"name": "Richard Arnold", "craft": "ISS"}], "number": 6, "message": "success"}

func nested() {
	// http://www.golangprograms.com/how-to-unmarshal-nested-json-structure.html

	jStr := `
    {
        "AAA":{
            "BBB": {
                "CCC": ["1111"],
                "DDD": ["2222", "3333"]
            }
        }
    }
    `

	type Inner struct {
		Key2 []string `json:"CCC"`
		Key3 []string `json:"DDD"`
	}
	type Outer struct {
		Key Inner `json:"BBB"`
	}
	type Outmost struct {
		Key Outer `json:"AAA"`
	}
	var cont Outmost
	json.Unmarshal([]byte(jStr), &cont)
	fmt.Printf("(1) %+v\n", cont)

}
