package main

import (
	"fmt"
	"net/http"
	"time"
)

func barhandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "this is LPK1 with time.Now()")
	t := time.Now()

	//	fmt.Fprintln(w, time.Now())
	fmt.Fprintln(w, t)
	// https://stackoverflow.com/questions/20234104/how-to-format-current-time-using-a-yyyymmddhhmmss-format
	const layout = "Monday, January 2, 2006 at 3:04pm (MST)"
	fmt.Fprintln(w, t.Format(layout)) // must be 2006?
}
